﻿function onReady() {

    function success(data) {
        data = JSON.parse(data);

        if ('success' in data) {
            if (data['success']) {
                if ('redirect' in data)
                    location.href = data['redirect'];
            } else {
                if ('message' in data) {
                    let msg = data['message'];
                    if (!msg) msg = 'Invalid username and/or password';
                    showError(msg);
                }
            }
        }
    }

    function checkEmail() {

        let email = $("#email").text();
        let error = $("#error-wrap");

        if (emailIsValid(email)) {
            error.slideUp();

            $.ajax({
                method: "POST",
                url: "/api/forgotpass",
                data: {
                    "email": email
                },
                error: error,
                success: function (data) { success(data); }
            });
        }
        else {
            error.slideDown();
        }
    }

    $("#btnRec").click(checkEmail);
}

$(onReady);