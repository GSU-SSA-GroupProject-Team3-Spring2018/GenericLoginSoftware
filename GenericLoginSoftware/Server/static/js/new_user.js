﻿

function onReady() 
{
    let questions_dict = [];

    function showError(msg) {
        $('#error-msg').text(msg);
        $('#error-wrap').slideDown();
    }

    function success(data) {
        data = JSON.parse(data);

        if ('success' in data) {
            if (data['success']) {
                if ('redirect' in data)
                    location.href = data['redirect'];
            } else {
                if ('message' in data) {
                    let msg = data['message'];
                    if (!msg) msg = 'Invalid username and/or password';
                    showError(msg);
                }
            }
        }
    }
    function SetQuestions(data) {
        questions_dict = jQuery.parseJSON(data);

        for (let i = 1; i <= 3; i++) {
            let q = $("#questions" + i);
            q.append("<option value=-1> - </option>")
            for (let j = 0; j < questions_dict.length; j++) {
                q.append("<option value=" + questions_dict[j].id + ">" + questions_dict[j].question + "</option>");
            }
        }
    }

    $.ajax({
        method: "GET",
        url: "/api/get-questions",
        data: {},
        success: function (data) { SetQuestions(data); }
    });

    function sendData() {
        let username = $("#username").val();
        let email = $("#email").val();
        let password = $("#pwd").val();
        let cemail = $("#cemail").val();
        let cpassword = $("#cpwd").val();
        let q1 = $('#questions1').find("option:selected").val();
        let q2 = $('#questions2').find("option:selected").val();
        let q3 = $('#questions3').find("option:selected").val();
        let q1a = $('#qanswer1').val();
        let q2a = $('#qanswer2').val();
        let q3a = $('#qanswer3').val();
        let error = $("#error");

        if (!usernameIsValid(username)) {
            error.text("Invalid username:\n - Must only use alphanumerics and period ., underscore _, and hyphen -\n - Length must be 4 characters or more");
            error.slideDown();
            return;
        }

        if (!passwordIsValid(password)) {
            error.text("Invalid password:\n - Must only use alphanumerics and ! [ ] + - : _ ?\n - Length must be 8 characters or more\n - Must contain at least one lowercase letter\n - Must contain at least one uppercase letter\n - Must contain at least one number");
            error.slideDown();
            return;
        }

        if (!emailIsValid(email)) {
            error.text("Invalid email:\n - Must follow valid email format: <email>@<domain>.<extension>");
            error.slideDown();
            return;
        }

        if (password != cpassword) {
            error.text("Passwords don't match");
            error.slideDown();
            return;
        }

        if (email != cemail) {
            error.text("Emails don't match");
            error.slideDown();
            return;
        }

        if (q1 == -1 || q2 == -1 || q3 == -1) {
            error.text("Please select 3 security questions");
            error.slideDown();
            return;
        }

        if (q1a == "" || q2a == "" || q3a == "") {
            error.text("Please enter answers for all three questions");
            error.slideDown();
            return;
        }

        error.slideUp();

        $.ajax({
            method: "POST",
            url: "/api/new-user",
            data: {
                "username": username,
                "password": password,
                "email": email,
                "q1": q1,
                "q2": q2,
                "q3": q3,
                "q1a": q1a,
                "q2a": q2a,
                "q3a": q3a
            },
            error: error,
            success: function (data) { success(data); }
        });
    }

    function success(data) {
        data = JSON.parse(data);

        if ('success' in data) {
            if (data['success']) {
                if ('redirect' in data)
                    location.href = data['redirect'];
            } else {
                if ('message' in data) {
                    let msg = data['message'];
                    if (!msg) msg = 'Invalid username and/or password';
                    showError(msg);
                }
            }
        }
    }
    
    $("#btn-submit").click(sendData);

    function HideChoiceInOthers(this_menu, selection) {
        for (let i = 1; i <= 3; i++) {
            if (i != this_menu) {
                other = $("#questions" + i);
                if (other.val() == selection || selection == -1) other.val(-1);
                $("#questions" + i + " > option[value=" + selection + "]").hide();
            }
        }
    }

    function DropDownChange() {
        return function () {
            for (let i = 1; i <= 3; i++) {
                $("#questions" + i).children().show();
            }
            
            for (let i = 1; i <= 3; i++) {
                selection = $("#questions" + i).val();
                if (selection != -1)
                    HideChoiceInOthers(i, selection);
            }
        }
    }
    
    for (let i = 1; i <= 3; i++) {
        $("#questions" + i).change(DropDownChange());
    }
}

$(onReady);