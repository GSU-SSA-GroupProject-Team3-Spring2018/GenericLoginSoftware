﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using ContentModels;
using ModelFields;

namespace Server
{

    // This class exists to encapsulate the route map
    class RouteMap
    {

        public static void SetRouteMap()
        {
            // Example configuration below
            // Configure this for final content using this as a template. The 404 page is automatically returned when a match is
            // not found (obviously), so don't configure that anywhere in here.

            // Set the url routes for the server
            Router.SetRoutes(
                 // root node
                 new RequestRoute(
                     // regex pattern
                     "",
                     // Array of child nodes to the root node
                     p_sub_routes: new RequestRoute[]
                     {
                         // URLs for access to different pages on our webserver
                         new RequestRoute(
                             @"^content/",
                             p_sub_routes: new RequestRoute[]
                             {
                                 new RequestRoute(
                                    @"^login[/]?",
                                    (req) => { return new ResponseData(Router.GetStaticFile("Login.html")); }
                                 ),
                                 new RequestRoute(
                                    @"^2fa[/]?",
                                    (req) => { return new ResponseData(Router.GetStaticFile("2FA.html")); }
                                 ),
                                 new RequestRoute(
                                    @"^forgot-password[/]?",
                                    (req) => { return new ResponseData(Router.GetStaticFile("Forgot_Password.html")); }
                                 ),
                                 new RequestRoute(
                                    @"^forgot-password_code[/]?",
                                    (req) => { return new ResponseData(Router.GetStaticFile("Forgot_Password_Code.html")); }
                                 ),
                                 new RequestRoute(
                                    @"^logged-in[/]?",
                                    (req) =>
                                    {
                                        // We use a cookie to store session tokens
                                        ContentModelInstance session = Util.GetUserSession(req);
                                        
                                        if (session != null)
                                            return new ResponseData(Router.GetStaticFile("Logged_In.html"));

                                        return new ResponseData(Router.GetStaticFile("Login.html"));
                                    }
                                 ),
                                 new RequestRoute(
                                    @"^new-user[/]?",
                                    (req) => { return new ResponseData(Router.GetStaticFile("New_User.html")); }
                                 ),
                                 new RequestRoute(
                                    @"^reset-password[/]?",
                                    (req) => { return new ResponseData(Router.GetStaticFile("Reset_Password.html")); }
                                 ),
                             }
                         ),

                         // API for logging in, creating new users, etc. through AJAX POST requests.
                         new RequestRoute(
                            @"^api/",
                            p_sub_routes: new RequestRoute[]
                            {
                                new RequestRoute(
                                    @"get-questions/?",
                                    (req) =>
                                    {
                                        var questions = AppModels.RecoveryQuestionChoice.All();
                                        string json = "[";
                                        int i = 0;
                                        foreach(var question in questions)
                                        {
                                            var sid = question.GetField("id").Value;
                                            var squestion = question.GetField("question").Value;
                                            json += "{";
                                            json += "\"id\": \""+sid+"\",";
                                            json += "\"question\": \""+squestion+"\"";
                                            if(i < questions.Count() - 1) json += "},";
                                            else json += "}";
                                            i++;
                                        }
                                        json += "]";
                                        return new ResponseData(json);
                                    }
                                ),
                                // API endpoint for logging a user in
                                new RequestRoute(
                                    @"^login[/]?",
                                    (req) =>
                                    {
                                        // Parse the POSTed form data
                                        Dictionary<string, string> parsed_form_data;
                                        string form_data = new StreamReader(req.InputStream, req.ContentEncoding).ReadToEnd();
                                        try
                                        {
                                            parsed_form_data = Router.ParseFormData(form_data);
                                        }
                                        // This exception occurs when form data is empty
                                        catch (IndexOutOfRangeException)
                                        {
                                            return new ResponseData("{\"success\": false,\"message\":\"Invalid format\"}");
                                        }
                                    
                                        // For a login request to be properly formed, it must provide a username and password
                                        if (parsed_form_data.ContainsKey("username") && parsed_form_data.ContainsKey("password"))
                                        {
                                            // Search for an existing session
                                            
                                            ContentModelInstance session = Util.GetUserSession(req);
                                            // Make sure that the session stored matches the credentials being used
                                            if (session != null)
                                            {
                                                ContentModelInstance session_user = session.GetField("user_id").GetRelated().First();
                                                if (session_user.GetField("username").Value != parsed_form_data["username"])
                                                {
                                                    Util.LogUserOut(session_user);
                                                }
                                                else
                                                    return new ResponseData("{\"success\": true,\"redirect\": \"/content/logged-in\"}");
                                            }
                                            
                                            try
                                            {
                                                // Search the database for an existing user with matching credentials
                                                ContentModelInstance user = AppModels.User.GetFirst(new Dictionary<string, string>
                                                {
                                                    { "username", parsed_form_data["username"] }
                                                });

                                                // If there exists a user, log the user in. 
                                                if (user != null)
                                                {
                                                    if (Util.CheckPasswordAgainstUser(user, parsed_form_data["password"]))
                                                    {
                                                        session = Util.LogUserInAndGetSession(user);
                                                        Dictionary<string, string> cookies = Util.GetUserSessionCookies(session);
                                                        // Notify the client-side AJAX handler that the user was logged in and the page should redirect
                                                        return new ResponseData("{\"success\": true,\"redirect\": \"/content/logged-in\"}", p_cookies: cookies);
                                                    }
                                                }
                                            }
                                            // An exception thrown by ContentModel.get_first when a query parameter is malformed
                                            catch (InvalidQueryParameterException) { }
                                        }

                                        // Notify the client-side AJAX handler that the login attempt failed and provide an error message
                                        return new ResponseData("{\"success\": false,\"message\":\"Wrong username and/or password\"}");
                                    }
                                ),
                                new RequestRoute(
                                    @"^logout[/]?",
                                    (req) =>
                                    {
                                        ContentModelInstance session = Util.GetUserSession(req);
                                        if (session != null)
                                            session.Delete();

                                        return new ResponseData("{\"success\": true, \"redirect\": \"/content/login\"}");
                                    }),
                                // API endpoint for creating a new user
                                new RequestRoute(
                                    @"^new-user[/]?",
                                    (req) => {
                                        // Parse the POSTed form data
                                        Dictionary<string, string> parsed_form_data;
                                        string form_data = new StreamReader(req.InputStream, req.ContentEncoding).ReadToEnd();
                                        try
                                        {
                                            parsed_form_data = Router.ParseFormData(form_data);
                                        }
                                        // This exception occurs when we try to parse an empty form
                                        catch (IndexOutOfRangeException)
                                        {
                                            return new ResponseData("{\"success\": false}");
                                        }

                                        try
                                        {
                                            // A new-user request must contain a username, password, and email to be well formed
                                            if (parsed_form_data.ContainsKey("username") && parsed_form_data.ContainsKey("password") && parsed_form_data.ContainsKey("email")
                                                && parsed_form_data.ContainsKey("q1") && parsed_form_data.ContainsKey("q2") && parsed_form_data.ContainsKey("q3")
                                                && parsed_form_data.ContainsKey("q1a") && parsed_form_data.ContainsKey("q2a") && parsed_form_data.ContainsKey("q3a"))
                                            {
                                                // Create a new user for the passed form data if one does not already exist
                                                ContentModelInstance user = AppModels.User.GetFirst(new Dictionary<string, string>
                                                {
                                                    { "username", parsed_form_data["username"] }
                                                });
                                                if (user != null)
                                                    return new ResponseData("{\"success\": false,\"message\":\"User already exists\"}");
                                                
                                                user = AppModels.User.GetFirst(new Dictionary<string, string>
                                                {
                                                    { "email", parsed_form_data["email"] }
                                                });
                                                if (user != null)
                                                    return new ResponseData("{\"success\": false,\"message\":\"Email already in use\"}");
                                                
                                                // If the user does not yet exist, create a new one and save it to the database
                                                ContentModelInstance new_user = new ContentModelInstance(AppModels.User, false);
                                                new_user.SetField("username", parsed_form_data["username"]);
                                                new_user.SetField("password_hash", parsed_form_data["password"]);
                                                new_user.SetField("email", parsed_form_data["email"]);
                                                new_user.Save();
                                                // User model refreshes itself in the after_save hook, so we have no need for an explicit refresh
                                                // new_user.Refresh();

                                                for (int i = 1; i <= 3; i++)
                                                {
                                                    ContentModelInstance q = new ContentModelInstance(AppModels.RecoveryQuestion);
                                                    q.SetField("user_id", new_user.GetField("id").Value);
                                                    q.SetField("recovery_question_choice_id", parsed_form_data["q" + i.ToString()]);
                                                    q.SetField("answer_hash", parsed_form_data["q" + i.ToString() + "a"]);
                                                    q.Save();
                                                }

                                                ContentModelInstance session = Util.LogUserInAndGetSession(new_user);

                                                Dictionary<string, string> cookies = Util.GetUserSessionCookies(session);

                                                // Notify the client-side AJAX handler that the new-user request succeeded and to redirect the the logged-in page
                                                return new ResponseData("{\"success\":true,\"redirect\":\"/content/logged-in\"}", p_cookies: cookies);
                                            }

                                            // Notify the client-side AJAX handler that the new-user request failed and provide an error message
                                            return new ResponseData("{\"success\": false,\"message\":\"Invalid username, email, and/or password\"}");
                                        }
                                        // Thrown when malformed data is set to the ModelFieldInstanceValue (does not match the format of the ModelField or contains SQL injection)
                                        catch(InvalidFieldDataException) { }
                                        catch(InvalidQueryParameterException e) {  }

                                        // Notify the client-side AJAX handler that the new-user request failed and provide an error message
                                        return new ResponseData("{\"success\": false,\"message\":\"Invalid username, email, and/or password\"}");
                                    }
                                ), new RequestRoute(
                                    @"^forgotpass[/]?",
                                    (req) =>
                                    {
                                        Dictionary<string, string> parsed_form_data;
                                        string form_data = new StreamReader(req.InputStream, req.ContentEncoding).ReadToEnd();
                                        try
                                        {
                                            parsed_form_data = Router.ParseFormData(form_data);
                                        }
                                        // This exception occurs when form data is empty
                                        catch (IndexOutOfRangeException)
                                        {
                                            return new ResponseData("{\"success\": false,\"message\":\"Invalid format\"}");
                                        }
                                        //make sure form contains an email to push through
                                        if (parsed_form_data.ContainsKey("email"))
                                        {
                                            try
                                            {
                                                // Search the database for an existing email with matching credentials
                                                ContentModelInstance email = AppModels.User.GetFirst(new Dictionary<string, string>
                                                {
                                                    { "email", parsed_form_data["email"] }
                                                });

                                                // If there exists a user, log the user in. 
                                                if (email != null)
                                                {
                                                    //resets password to email server, and sets password for the user to the generated security key

                                                    //
                                                    // TODO: Call the email client when done
                                                    //

                                                    // Notifying the client to move onto the Forgot_password_code screen
                                                    return new ResponseData("{\"success\": true,\"redirect\": \"/content/Forgot_Password_Code\"}");
                                                }
                                            }
                                            // An exception thrown by ContentModel.get_first when a query parameter is malformed
                                            catch (InvalidQueryParameterException) { }
                                        }

                                        // Notify the client-side AJAX handler that the email request attempt failed and provide an error message
                                        return new ResponseData("{\"success\": false,\"message\":\"Email is not valid or does not exist\"}");
                                    }
                                ),
                            }
                        ),

                        new RequestRoute(
                            @"^reset_password[/]?",
                            (req) =>
                            {
                                // Parse the POSTed form data
                                Dictionary<string, string> parsed_form_data;
                                string form_data = new StreamReader(req.InputStream, req.ContentEncoding).ReadToEnd();
                                try
                                {
                                    parsed_form_data = Router.ParseFormData(form_data);
                                }
                                // This exception occurs when form data is empty
                                catch (IndexOutOfRangeException)
                                {
                                    return new ResponseData("{\"success\": false,\"message\":\"Invalid format\"}");
                                }
                                    
                                // For a reset password request to be properly formed, it must provide a password.
                                if (parsed_form_data.ContainsKey("password"))
                                {
                                    try
                                    {
                                        //TODO:  Must retrieve info from previous page sessions.
                                        //       Must verify passwords meet criterium.
                                        // Search the database for an existing user with matching credentials
                                        ContentModelInstance user = AppModels.User.GetFirst(new Dictionary<string, string>
                                        {
                                            { "username", parsed_form_data["username"] }
                                        });

                                        // If there exists a user, update the password. 
                                        if (user != null)
                                        {
                                            //Updating Password.
                                            user.SetField("password_hash", parsed_form_data["password"]);
                                            user.Save();

                                            // Page should redirect to the login after a successful password reset.
                                            return new ResponseData("{\"success\": true,\"redirect\": \"/content/login\"}");
                                        }
                                    }
                                    // An exception thrown by ContentModel.get_first when a query parameter is malformed
                                    catch (InvalidQueryParameterException) { }
                                }

                                // Notify the client-side AJAX handler that the login attempt failed and provide an error message
                                return new ResponseData("{\"success\": false,\"message\":\"Something went wrong.\"}");
                            }
                        ),

                         // Static files configuration
                         new RequestRoute(
                            @"^static/(js|css)/",
                            (req) => {
                                try
                                {
                                    // Trim the static prefix and find the static file requested
                                    string staticfile = req.Url.AbsolutePath.Replace("/static/", "");
                                    return new ResponseData(Router.GetStaticFile(staticfile));
                                }
                                catch (FileNotFoundException)
                                {
                                    // If we don't find the static file requested, we return a 404 as with other bad requests
                                    throw new NoMatchInRequestRouteException();
                                }
                            }
                         )
                     }
                 )
            );
        }
    }
}
