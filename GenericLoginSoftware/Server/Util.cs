﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using ContentModels;
using ModelFields;

namespace Server
{
    class Util
    {

        public static ContentModelInstance GetUserSession(HttpListenerRequest req)
        {
            foreach (Cookie c in req.Cookies)
            {
                if (c.Name.StartsWith(Config.SessionKeyPrefix))
                {
                    string key = c.Name.Substring(Config.SessionKeyPrefix.Length, c.Name.Length - 1);
                    string token = c.Value;
                    ContentModelInstance session = AppModels.Session.GetFirst(new Dictionary<string, string>
                    {
                        { "key", key },
                        { "token", token }
                    });
                    return session;
                }
            }

            return null;
        }

        public static Dictionary<string, string> GetUserSessionCookies(ContentModelInstance session)
        {
            Dictionary<string, string> mixed_cookie_vals = new Dictionary<string, string>();
            for (int i = 0; i < Config.NumberOfFakeCookies; i++)
            {
                string new_key = GetCryptographicallySecureB64String(Config.SessionKeyPrefix.Length + 16,
                    // Replace characters which can mess up cookie formatting
                    replace: new Dictionary<char, char> {
                        { '/', 'a' },
                        { '=', 'b' },
                        { '+', 'c' },
                        { '\\', 'd' }
                    });
                // Perform a simple modular addition to avoid non-token keys having token key formatting.
                if (new_key.StartsWith(Config.SessionKeyPrefix)) new_key = (char)((new_key.ElementAt(0) + 1) % sizeof(char)) + new_key.Substring(1, new_key.Length - 1);
                string new_value = GetCryptographicallySecureB64String(32,
                    // Replace characters which can mess up cookie formatting
                    replace: new Dictionary<char, char> {
                        { '/', 'a' },
                        { '=', 'b' },
                        { '+', 'c' },
                        { '\\', 'd' }
                    });
                mixed_cookie_vals.Add(new_key, new_value);
            }
            mixed_cookie_vals.Add(Config.SessionKeyPrefix + session.GetField("key").Value, session.GetField("token").Value);

            return mixed_cookie_vals;
        }

        public static bool CheckPasswordAgainstUser(ContentModelInstance user, string password)
        {
            string salt = user.GetField("salt").Value;
            bool exists_in_database = user.ExistsInDatabase;
            string processed_password = ((HashField)AppModels.User.fields["password_hash"]).ProcessValue(password, user, validate: false);
            return user.GetField("password_hash").Value == processed_password;
        }

        public static ContentModelInstance LogUserInAndGetSession(ContentModelInstance user)
        {
            ContentModelInstance session = AppModels.Session.GetFirst(new Dictionary<string, string>
            {
                { "user_id", user.GetField("id").Value }
            });

            DateTime now = DateTime.Now;
            string refresh_token_at = new NpgsqlTypes.NpgsqlDateTime(now + Config.SessionTokenExpiration).ToString();
            string idle_timeout = new NpgsqlTypes.NpgsqlDateTime(now + Config.SessionIdleTimeout).ToString();

            if (session == null)
            {
                session = new ContentModelInstance(AppModels.Session, false);
                session.SetField("user_id", user.GetField("id").Value);

                string key = GetCryptographicallySecureB64String(16,
                    // Replace characters which can mess up cookie formatting
                    replace: new Dictionary<char, char> {
                        { '/', 'a' },
                        { '=', 'b' },
                        { '+', 'c' },
                        { '\\', 'd' }
                    });
                string token = GetCryptographicallySecureB64String(32,
                    // Replace characters which can mess up cookie formatting
                    replace: new Dictionary<char, char> {
                        { '/', 'a' },
                        { '=', 'b' },
                        { '+', 'c' },
                        { '\\', 'd' }
                    });

                session.SetField("key", key);
                session.SetField("token", token);
            }

            session.SetField("refresh_token_at", refresh_token_at);
            session.SetField("idle_timeout", idle_timeout);
            session.Save();

            return session;
        }

        public static void LogUserOut(ContentModelInstance user)
        {
            ContentModelInstance session = AppModels.Session.GetFirst(new Dictionary<string, string>
            {
                { "user_id", user.GetField("id").Value }
            });

            session.Delete();
        }

        public static string GetCryptographicallySecureB64String(int num_chars, Dictionary<char, char> replace = null)
        {
            string val;
            using (RandomNumberGenerator rng = new RNGCryptoServiceProvider())
            {
                // 1 b64 character represents 6 bits of information, so number of bytes represented by n chars is 3 * n / 4
                int num_bytes = (int)(3 * Math.Ceiling((double)num_chars / 4));
                byte[] bytes = new byte[num_bytes];
                rng.GetBytes(bytes);
                val = Convert.ToBase64String(bytes);
            }

            if (replace != null)
                foreach (KeyValuePair<char, char> kvp in replace)
                    val = val.Replace(kvp.Key, kvp.Value);

            // byte padding in a Base64 string can cause the length to be a little off, so just chop off the extra here at the end.
            return val.Substring(0, num_chars);
        }
    }
}
