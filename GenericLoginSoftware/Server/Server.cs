﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{

    class Server
    {
        public static readonly Server instance = new Server();
        
        private bool running = true;
        private Mutex running_mutex = new Mutex();
        private List<Worker> workers = new List<Worker>();
        private uint num_workers;
        private uint _worker_manager_loop_granularity;
        private string[] ips;
        private uint top_port;

        private Server() { }

        public void Configure()
        {
            top_port = Config.StartingPort;
            num_workers = Config.NumberOfWorkers;
            _worker_manager_loop_granularity = Config.WorkerManagerLoopGranularity;

            // TODO: Replace hard-coded backend IP's with configurable alternative
            ips = new string[]
            {
                "127.0.0.1",
                "localhost"
            };
        }

        public void Run()
        {
            // Launch server functionality in a seperate thread so we can process local command line input at the same time
            ThreadStart worker_manager_loop_start = instance.worker_manager_loop;
            Thread worker_manager_thread = new Thread(worker_manager_loop_start);
            worker_manager_thread.Start();

            // Initial message
            Console.WriteLine("Login Server started on IP " + ips + " with starting port " + Convert.ToString(Config.StartingPort) + " and " + Convert.ToString(Config.NumberOfWorkers) + " workers.");
            Console.WriteLine("q, quit, exit: Stop the server");
            Console.WriteLine("list-workers: List current worker threads and their associated port numbers\n");
            //Console.WriteLine("add-workers <int>: Add <int> more workers\n");

            // Local boolean to track the running state of the server
            running_mutex.WaitOne();
            bool local_running = running;
            running_mutex.ReleaseMutex();

            // Get command line input
            while (local_running)
            {
                Console.Write("> ");
                string s = Console.ReadLine();

               ExecuteCommand(s);

                // Get running state
                running_mutex.WaitOne();
                local_running = running;
                running_mutex.ReleaseMutex();
            }
        }

        // Creates and destroys workers based on num_workers. Manages port distribution.
        // TODO: Test that a port number is open before giving it to a worker. Store skipped ports for backtracking later.
        private void worker_manager_loop()
        {
            // Initialize a local boolean to track the running state of the server
            running_mutex.WaitOne();
            bool local_running = running;
            running_mutex.ReleaseMutex();

            while (local_running)
            {
                // Spawn or kill workers. Each worker gets its own port. Responds to add-workers if we decide to put that back in.
                if (workers.Count < num_workers)
                {
                    workers.Add(new Worker(ips, top_port++));
                    workers.Last().Start();
                }
                if (workers.Count > num_workers)
                {
                    workers.Last().Kill(false);
                    top_port--;
                }

                Thread.Sleep((int)_worker_manager_loop_granularity);

                running_mutex.WaitOne();
                local_running = running;
                running_mutex.ReleaseMutex();
            }

            Console.ReadKey();
        }

        // Returns false when the worker_manager_loop should stop executing
        private void ExecuteCommand(string command)
        {
            string[] command_split = command.Split(new char[] { ' ' });
            string command_name = command_split[0];
            switch (command_name.ToLower())
            {
                // Quit
                case "q": case "quit":
                case "exit":
                    {
                        // Let the worker manager thread know that we are shutting down
                        running_mutex.WaitOne();
                        running = false;
                        running_mutex.ReleaseMutex();

                        // Shutdown all the workers currently running
                        KillAllWorkers(false);
                        Console.WriteLine("Server successfully shutdown");
                        Console.WriteLine("Press any key to exit...");
                        break;
                    }

                case "list-workers":
                    {
                        foreach (Worker w in workers)
                        {
                            Console.WriteLine(w.ToString());
                        }

                        break;
                    }

                // add-workers would require an Apache restart. Would be a cool admin command, but I'm commenting it out.

                /*case "add-workers":
                    {
                        if (command_split.Length == 2)
                        {
                            try
                            {
                                uint arg = Convert.ToUInt16(command_split[1]);
                                num_workers += arg;
                                Console.WriteLine(Convert.ToString(arg) + " workers added.");
                            }
                            catch (FormatException)
                            {
                                Console.Error.WriteLine("add-workers requires one integer argument");
                            }
                            catch (OverflowException)
                            {
                                Console.Error.WriteLine("add-workers argument required to be < 2^16");
                            }
                        }
                        
                        break;
                    }*/
                default:
                    break;
            }
        }

        private void KillAllWorkers(bool urgent)
        {
            foreach (Worker w in workers)
            {
                w.Kill(urgent);
            }
        }

    }

}
