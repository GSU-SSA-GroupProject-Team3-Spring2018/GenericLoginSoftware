# NEW: Responsibility options - See also merge request for Brett_InitializeServerFunctionality
1. Router.cs
  * Should return the requested content to the user
  * Should make use of DBInterface to access data
  * Make sure that data passed in from requests is POSTed in HTML forms and not given as a GET query parameter (/url/path?param=val)
2. DBBackend.cs
  * Should offer access to PostgreSQL
  * Should utilize proper authentication (*Can* be postponed until after code review)
4. ContentModels.cs
  * Should define C# representations of the data models that will live in our database
  * All models should derive from ContentModels.ContentModel and utilize the column map provided in the base class to define table structure
5. ModelFields.cs
  * Should provide self-validating model fields for mapping to the database
  * All fields should derive from ModelFields.ModelField
3. HTML content
  * See the list of pages Jesse posted in Google Drive
  * Make sure that your requests interface with the url map defined in url_map.conf
  * Make sure that your requests containing credentials use POST instead of GET as described in the Router.cs responsibility list
4. Unit tests
  * Unit tests need to be drawn up for the major components of the server.
  * Use the project already created to do this
5. Attack test cases
  * Draw up outsider attack schemes
  * These attack schemes preferably should only use knowledge about our exposed infrastructure, not necessarily code details.
  * After generalized attack schemes have been created, code execution should be used to identify internal weaknesses.

### Project structure

* GenericLoginSoftware
  * Server - Code for our backend
  * Server_Test - Code for unit tests
  * Deliverable - A folder containing Apache 2.4.33, configuration and setup files, and a README.txt on installing and running Apache.  
    A compiled version of our server will eventually end up here, as well as our static files and setup and a pre-made database for PostgreSQL.  
	For now, run the C# server from Visual Studio.

### Installing and running Apache. This has to come before running the project in VS

1. Run Powershell as admin
2. cd into \<project folder\>\\Deliverable\\Team3SoftwarePackage
3. Run ./install
4. Run ./propagate_config
5. Run ./start
6. If you want to remove the service created in ./install, there is also ./uninstall
7. This folder also contains README.txt which has these instructions in it, and login.conf which has crossover configuration for Apache (gets reformatted by propagate_config) and our backend.

### Config guide

* Everyone needs to set StaticFileDirectory in your login.conf for Router to be able to find your static files

## Git setup

1. [Download git for Windows](https://git-scm.com/download/win)
  * During installation, I would recommend that you leave everything default except for terminal selection. Just use Git Bash for your terminal so that your Windows commands do not get cluttered with Unix commands.
  * I'm not sure if SSH key generation is optional or not. If you cannot push with HTTPS, generate an ssh-rsa key pair (I used ssh-keygen in the Git Bash terminal) and add the public key to your GitLab account. Visual Studio will automatically use the ssh key from __C:/Users/[username]/.ssh/__ (may require a restart of VS).
2. Open Visual Studio and click Team > Manage Connections...
3. In the __Team Explorer - Connect__ pane (right-hand side), look for the __Local Git Repositories__ entry. Select __Clone__ and enter the HTTPS URL of this repository -> https://gitlab.com/GSU-SSA-GroupProject-Team3-Spring2018/GenericLoginSoftware.git.
4. After you have the repository cloned into __C:/Users/[username]/source/repos/GenericLoginSoftware/__, click the entry for the project in the __Team Explorer - Connect__ pane.
5. Click __Settings > Global Settings__ and enter your user name and email address for your GitLab account.

### If you haven't used Git with Visual Studio and want an example, use the following workflow guides to make your name *italicized with asterisks*

* *Brett Mitchell*
* Jamie Hayes
* Jesse Minton
* *Abraham Pena*
* Anthony Shedlowski
* Corey McCoy

### Before making edits

1. Click the branch button (double arrow icon) in the bottom right corner
2. Make ABSOLUTELY sure that you have the master branch checked out when you branch unless you are branching for your own benefit. If you do opt to create sub-branches, merge them back into your feature branch before merging back to master.
2. Fill in __Enter a branch name \<Required\>__. Branch naming convention: \<your name\>\_\<what you are working on\>. Example: Brett_MarkNameComplete
3. Make sure to keep __Checkout branch__ checked and create the branch

### After making edits

1. Click the commit button (pencil icon) near the branch button.
2. Fill out a commit message. Use imperative sentences: "Add asterisks to name in README.md", not "I added..." or "Added..."
3. Commit all each time unless you have a specific reason to split your commits up.
5. Let your edits pertaining to one change pile up before you commit so we can have a relatively clean commit history.
5. Click the up push button (arrow icon) next to the commit button.
6. In the __Synchronization__ pane, click __Outgoing Commits > Push__.
