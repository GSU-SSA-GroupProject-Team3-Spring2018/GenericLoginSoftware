﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

using ContentModels;

namespace Server
{

    // Helper class for use with DBBackend.
    // Implements IEnumerator and IEnumerable so we can use DataSet in a foreach loop
    public class DataSet : IEnumerable<ContentModelInstance>
    {
        public enum FilterType
        {
            EXCLUDE,
            INCLUDE,
            CONTAINS
        }

        // Store multiple rows of data
        private readonly List<ContentModelInstance> data = new List<ContentModelInstance>();

        public DataSet(List<ContentModelInstance> p_data)
        {
            data = p_data;
        }

        public DataSet() { }

        public void AddContentModel(ContentModelInstance cm)
        {
            data.Add(cm);
        }

        public ContentModelInstance Get(int idx)
        {
            return data.ElementAt(idx);
        }

        public DataSet Filter(Dictionary<string, string> filters, FilterType filter_type)
        {
            foreach (KeyValuePair<string, string> filter in filters)
            {
                // filter.Key is the column name and filter.Value is the string to match against the data
                // Filter out rows for which {filter.Key <filter_type operation using filter.Value> data row} is true
            }

            return this;
        }

        public int Count()
        {
            return data.Count();
        }

        // Return a read only enumerator to use in a ForEach loop
        public IEnumerator<ContentModelInstance> GetEnumerator()
        {
            return data.AsReadOnly().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }

    // DBBackend provides a facade style interface to the PostgreSQL database
    class DBBackend
    {
        // Should execute a query and format the resulting data in a DataSet object.
        // T is the type of the content model in the DataSet
        public static DataSet Query(string query, ContentModel type)
        {
            DataSet ds = new DataSet();

            try
            {
                var connString = String.Format("Host={0};Username={1};Password={2};Database={3}",
                                               Config.DatabaseHostName, /**localhost(probably)**/
                                               Config.DatabaseUsername,
                                               Config.DatabasePassword,
                                               Config.DatabaseName);

                using (var conn = new NpgsqlConnection(connString))
                {
                    conn.Open();
                    
                    using (var cmd = new NpgsqlCommand(query, conn))
                    using (var reader = cmd.ExecuteReader())
                    {
                        // Get the column names (each one corresponds to a field in the ContentModel)
                        List<string> field_names = new List<string>();

                        for (int i = 0; i < reader.FieldCount; i++)
                            field_names.Add(reader.GetName(i));

                        while (reader.Read())
                        {
                            // Instantiate a new content model for the return collection
                            ContentModelInstance inst = new ContentModelInstance(type, true);
                            for (int i = 0; i < reader.FieldCount; i++)
                                inst.SetFieldLiteral(field_names[i], reader[i].ToString());

                            ds.AddContentModel(inst);
                        }
                        conn.Close();
                    }
                }
            }
            catch (Exception e) { Console.WriteLine(e.ToString()); }

            return ds;
        }

        /**
         * Function to use when adding a new user
         * where you're not requiring an response
         * to avoid bad data being send to the database this should not be 
         * Used 
         **/
        public static void NonQuery(string query)
        {
            try
            {
                //connection string
                var connString = String.Format("Host={0};Username={1};Password={2};Database={3}",
                                               Config.DatabaseHostName, /**localhost(probably)**/
                                               Config.DatabaseUsername,
                                               Config.DatabasePassword,
                                               Config.DatabaseName);
                //opening db connection
                using (var conn = new NpgsqlConnection(connString))
                {
                    conn.Open();

                    //Inserts data
                    using (var cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = query;
                        cmd.ExecuteNonQuery();
                    }
                    //close connection
                    conn.Close();
                }
            }
            catch (Exception)
            {

            }
        }

        /*public static void NonQuery(List<NpgsqlParameter> query_params)
        {
            try
            {
                //connection string
                var connString = String.Format("Host={0};Username={1};Password={2};Database={3}",
                                               Config.DatabaseHostName, //localhost(probably)
                                               Config.DatabaseUsername,
                                               Config.DatabasePassword,
                                               Config.DatabaseName);
                //opening db connection
                using (var conn = new NpgsqlConnection(connString))
                {
                    conn.Open();

                    //Inserts data
                    using (var cmd = new NpgsqlCommand())
                    {
                        foreach (NpgsqlParameter param in query_params)
                        {
                            cmd.Parameters.Add(param);
                        }
                        cmd.Connection = conn;
                        cmd.ExecuteNonQuery();
                    }
                    //close connection
                    conn.Close();
                }
            }
            catch (Exception)
            {

            }
        }*/
    }
}
