﻿using System;
using System.Collections.Immutable;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ModelFields;
using Server;

namespace ContentModels
{

    class NonexistantModelFieldException : NullReferenceException
    {
        public NonexistantModelFieldException(string message) : base(message) { }
    }

    class InvalidQueryParameterException : Exception
    {
        public InvalidQueryParameterException(string message) : base(message) { }
    }

    public class ContentModelInstance
    {
        private ContentModel type;

        private Dictionary<string, object> nonpersisted_attributes = new Dictionary<string, object>();
        private Dictionary<string, ModelFieldInstanceValue> fields = new Dictionary<string, ModelFieldInstanceValue>();
        // Provide immutable access to fields
        internal ImmutableDictionary<string, ModelFieldInstanceValue> GetFields() { return fields.ToImmutableDictionary(); }

        public bool ExistsInDatabase { get; private set; }

        public ContentModelInstance(ContentModel p_type) : this(p_type, false) { }
        public ContentModelInstance(ContentModel p_type, bool p_exists_in_database)
        {
            type = p_type;
            ExistsInDatabase = p_exists_in_database;

            // Create ModelFieldInstanceValues for each field definition
            foreach (KeyValuePair<string, ModelField> kvpair in type.fields)
            {
                fields.Add(kvpair.Key, new ModelFieldInstanceValue(kvpair.Value));
            }
        }

        // Validate the data in the fields in
        protected bool ValidateFields()
        {
            return true;
        }

        // Create a new record for a ContentModel not yet in the database
        private void Insert()
        {
            ExistsInDatabase = true;
            // use DBBackend to insert a new row for this instance
            string query = "INSERT INTO " + type.TableName + " (";
            string comma = "";
            string value_string = "";
            foreach (ModelFieldInstanceValue field in fields.Values)
            {
                // Auto fields are not explicitly set
                if (field.RelatedField.Auto) continue;

                query += comma + field.RelatedField.ColumnName;
                value_string += comma + "\'" + field.Value + "\'";
                comma = ", ";
            }

            query += ")\nVALUES (" + value_string + ");";

            DBBackend.NonQuery(query);
        }

        // Update the record for a ContentModel that is in the database
        private void Update()
        {
            string query = "UPDATE " + type.TableName + "\nSET ";
            string set_clause = "";
            string where_clause = "WHERE ";
            string comma = "";
            string and = "";
            foreach (ModelFieldInstanceValue field in fields.Values)
            {
                if (!field.RelatedField.Auto && field.Changed)
                {
                    set_clause += comma + field.RelatedField.ColumnName + "=\'" + field.Value + "\'";
                    comma = ", ";
                }
                if (field.RelatedField.IsPK)
                {
                    where_clause += and + field.RelatedField.ColumnName + "=\'" + field.Value + "\'";
                    and = " AND ";
                }
            }
            // If nothing changed, then don't perform a query
            if (set_clause == "") return;

            query += set_clause + "\n" + where_clause + ";";

            DBBackend.NonQuery(query);
        }

        // Save this ContentModel to the database using DBBackend
        public void Save()
        {
            type.before_save(this);

            if (ExistsInDatabase)
                Update();
            else
                Insert();

            type.after_save(this);
        }

        // Delete this ContentModel from the database using DBBackend
        public void Delete()
        {
            if (!ExistsInDatabase)
                return;

            ExistsInDatabase = false;

            string query = "DELETE FROM " + type.TableName + "\nWHERE ";
            
            string and = "";
            foreach (ModelFieldInstanceValue field in fields.Values)
            {
                if (field.RelatedField.IsPK)
                {
                    query += and + field.RelatedField.ColumnName + "=\'" + field.Value + "\'";
                    and = " AND ";
                }
            }

            query += ";";

            DBBackend.NonQuery(query);
        }

        public ModelFieldInstanceValue GetField(string column_name)
        {
            ModelFieldInstanceValue field = fields[column_name];
            if (field == null)
                throw new NonexistantModelFieldException("The field " + column_name + " does not exist.");
            return field;
        }

        public void SetField(string column_name, string value)
        {
            ModelFieldInstanceValue field = GetField(column_name);
            // No try/catch. We want to allow the exception to be caught and processed elsewhere.
            field.Set(value, this);
        }

        public void SetFieldLiteral(string column_name, string value)
        {
            ModelFieldInstanceValue field = GetField(column_name);
            // No try/catch. We want to allow the exception to be caught and processed elsewhere.
            field.SetLiteral(value);
        }

        public void Refresh()
        {
            Dictionary<string, string> query_params = new Dictionary<string, string>();
            foreach (ModelFieldInstanceValue mfi in GetFields().Values)
            {
                if ((mfi.RelatedField.Unique || mfi.RelatedField.IsPK) && !mfi.RelatedField.Auto)
                    query_params.Add(mfi.RelatedField.ColumnName, mfi.Value);
            }
            ContentModelInstance updated_instance = type.GetFirst(query_params);
            foreach (ModelFieldInstanceValue mfi in updated_instance.GetFields().Values)
            {
                SetFieldLiteral(mfi.RelatedField.ColumnName, mfi.Value);
            }
        }

        public object GetAttribute(string key)
        {
            if (nonpersisted_attributes.ContainsKey(key)) return nonpersisted_attributes[key];

            return null;
        }

        public void SetAttribute(string key, object value)
        {
            if (nonpersisted_attributes.ContainsKey(key))
                nonpersisted_attributes[key] = value;
            else
                nonpersisted_attributes.Add(key, value);
        }
    }

    // An abstract base class for all content models. It provides a sorted set for storing fields and save and delete methods for creating, altering and removing 
    public class ContentModel
    {

        public string TableName { get; protected set; } = "ContentModel";

        // This dictonary is where the structure of a table is defined.
        internal Dictionary<string, ModelField> fields = new Dictionary<string, ModelField>();
        internal Action<ContentModelInstance> before_save;
        internal Action<ContentModelInstance> after_save;

        // Require the ContentModel instance to provide whether or not it already exists in the database
        public ContentModel(string p_table_name,
                            List<ModelField> p_fields,
                            Action<ContentModelInstance> p_before_save = null,
                            Action<ContentModelInstance> p_after_save = null)
        {
            TableName = p_table_name;

            foreach (ModelField field in p_fields)
            {
                field.SetParentContentModel(this);
                fields.Add(field.ColumnName, field);
            }

            before_save = p_before_save ?? ((c) => { });
            after_save = p_after_save ?? ((c) => { });
        }

        private string get_query_string_params(Dictionary<string, string> query_params)
        {
            string stringified_params = "";
            string and = "";
            foreach (KeyValuePair<string, string> kvpair in query_params)
            {
                // Check that we are not trying to select by a non-existent column name
                if (!fields.ContainsKey(kvpair.Key))
                    throw new InvalidQueryParameterException("No field " + kvpair.Key + " in model " + TableName);

                // Ensure that the where clause value does not contain SQL injection attempts
                if (!ModelField.IsSQLClean(kvpair.Value))
                    throw new InvalidQueryParameterException("The query parameter:\n\n" + kvpair.Value + "\n\n is not clean");

                ModelField field_def = fields[kvpair.Key];
                stringified_params += and + field_def.ColumnName + "=\'" + kvpair.Value + "\'";

                and = " AND ";
            }
            
            return stringified_params;
        }

        // Use this if you need all the entries in the dataset
        public DataSet Get(Dictionary<string, string> query_params)
        {
            string query = "SELECT *\nFROM " + TableName + "\nWHERE " + get_query_string_params(query_params);

            return DBBackend.Query(query, this);
        }

        // Use this if you only need the first entry in the resulting dataset
        public ContentModelInstance GetFirst(Dictionary<string, string> query_params)
        {
            string query = "SELECT *\nFROM " + TableName + "\nWHERE " + get_query_string_params(query_params) + "\nFETCH FIRST 1 ROW ONLY;";
            DataSet res = DBBackend.Query(query, this);
            if (res.Count() > 0)
                return res.First();
            return null;
        }

        public DataSet All()
        {
            string query = "SELECT *\nFROM " + TableName + ";";
            return DBBackend.Query(query, this);
        }
    }
}
