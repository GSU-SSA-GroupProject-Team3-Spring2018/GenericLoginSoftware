
$current_dir=$MyInvocation.MyCommand.Path -replace "\\[^\\]*\.ps1$", "\"

Write-Output "Uninstalling Apache"
./Apache24/bin/httpd.exe -k uninstall -n "ApacheServiceTeam3Login"
Write-Output "`n"

Write-Output "Uninstalling PostgreSQL"
C:\PostgreSQL\uninstall\uninstall.exe | Out-Null
