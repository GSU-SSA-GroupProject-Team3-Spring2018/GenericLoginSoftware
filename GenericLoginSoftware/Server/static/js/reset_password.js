﻿

function onReady() {

    function showError(msg) {
        $('#error-msg').text(msg);
        $('#error-wrap').slideDown();
    }

    function success(data) {
        data = JSON.parse(data);

        if ('success' in data) {
            if (data['success']) {
                if ('redirect' in data)
                    location.href = data['redirect'];
            } else {
                if ('message' in data) {
                    let msg = data['message'];
                    if (!msg) msg = 'Invalid username and/or password';
                    showError(msg);
                }
            }
        }
    }

    function onResetClick() {

        let password = $("#pwd").text();
        let cpassword = $("#cpwd").text();
        let error = $("#error-wrap");

        if (passwordIsValid(password) && password != cpassword) {
            error.slideUp();

            $.ajax({
                method: "POST",
                url: "/api/reset_password",
                data: {
                    "password": password,
                },
                error: error,
                success: function (data) { success(data); }
            });
        }
        else {
            error.slideDown();
        }

    }

    $("#reset-button").click(onResetClick);

}

$(onReady);
