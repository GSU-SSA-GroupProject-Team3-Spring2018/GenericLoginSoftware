
Write-Output "Starting Apache server"
./Apache24/bin/httpd.exe -k start -n "ApacheServiceTeam3Login"
Write-Output "`n"

Write-Output "Starting Postgres server"
C:\PostgreSQL\pg10\bin\pg_ctl start -D "C:\PostgreSQL\data\pg10"
Write-Output "`n"

Write-Output "Starting Team 3 Login Server"
./bin/Server.exe
Write-Output "`n"