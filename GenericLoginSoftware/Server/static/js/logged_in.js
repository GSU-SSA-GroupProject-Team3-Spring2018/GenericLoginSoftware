﻿
function onReady() {

    function success(data) {
        data = JSON.parse(data);

        if ('success' in data) {
            if (data['success']) {
                if ('redirect' in data)
                    location.href = data['redirect'];
            } else {
                location.href = "/content/login";
            }
        }
    }

    function onLogoutClick() {

        $.ajax({
            method: "POST",
            url: "/api/logout",
            error: error,
            success: function (data) { success(data); }
        });

    }

    $("#btn-logout").click(onLogoutClick);

}

$(onReady);
