﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using ContentModels;
using Server;
using NpgsqlTypes;

namespace ModelFields
{

    public class NotAForeignKeyException : InvalidOperationException
    {
        public NotAForeignKeyException(string message) : base(message) { }
    }

    public class InvalidFieldDataException : InvalidOperationException
    {
        public InvalidFieldDataException(string message) : base(message) { }
    }

    public class SQLInjectionDetectedException : InvalidFieldDataException
    {
        public SQLInjectionDetectedException(string message) : base(message) { }
    }

    public class ModelFieldInstanceValue
    {
        public bool Changed { get; private set; } = false;

        public ModelField RelatedField { get; private set; }
        public string Value { get; private set; }

        public void Set(string p_value, ContentModelInstance inst)
        {
            if (RelatedField.Auto) return;
            Changed = true;
            Value = RelatedField.ProcessValue(p_value, inst);
        }

        public void SetLiteral(string p_value)
        {
            Value = p_value;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public ModelFieldInstanceValue(ModelField p_field)
        {
            RelatedField = p_field;
        }

        // If this ModelField is a foreign key, get the ContentModel related by this ModelField
        public DataSet GetRelated()
        {
            if (!RelatedField.IsFK) throw new NotAForeignKeyException(RelatedField.ColumnName + " is not a foreign key");
            string table = RelatedField.ForeignKeyField.ParentContentModel.TableName;
            string column = RelatedField.ForeignKeyField.ColumnName;
            string query = "SELECT * FROM " + table + "\nWHERE " + column + " = \'" + Value + "\';";
            return DBBackend.Query(query, RelatedField.ForeignKeyField.ParentContentModel);
        }
    }

    public abstract class ModelField
    {
        public bool Auto { get; protected set; } = false;
        public bool Unique { get; protected set; } = false;
        public string ColumnName { get; private set; }
        public string PostgresqlDatatype { get; private set; }

        public bool IsPK { get; private set; }
        public bool IsFK { get; private set; }
        public ModelField ForeignKeyField { get; private set; }

        public ContentModel ParentContentModel { get; private set; }

        internal Func<string, ContentModelInstance, string> instance_specific_action;
        internal Func<string, string> field_specific_action;
        internal Func<string, ContentModelInstance, bool> instance_specific_validator;

        public ModelField(
            string p_column_name,
            string p_postgresql_datatype,
            bool p_is_primary_key,
            bool p_is_foreign_key,
            bool p_is_unique = false,
            ModelField p_foreign_table_name = null,
            Func<string, ContentModelInstance, string> p_instance_specific_action = null,
            Func<string, string> p_field_specific_action = null,
            Func<string, ContentModelInstance, bool> p_instance_specific_validator = null)
        {
            ColumnName = p_column_name;
            PostgresqlDatatype = p_postgresql_datatype;
            IsPK = p_is_primary_key;
            IsFK = p_is_foreign_key;
            // Primary keys are always unique, so regardless of unique setting, Unique must be true when IsPK is true.
            Unique = p_is_primary_key || p_is_unique;
            ForeignKeyField = p_foreign_table_name;
            instance_specific_action = p_instance_specific_action ?? ((v, i) => { return v; });
            field_specific_action = p_field_specific_action ?? ((v) => { return v; });
            instance_specific_validator = p_instance_specific_validator ?? ((v, i) => { return true; });
        }

        internal string ProcessValue(string p_value, ContentModelInstance inst, bool validate = true)
        {
            bool valid = false;
            // Sanitization check. This just throws an exception on invalid data; it does not modify the value.
            if (validate)
                valid = ModelField.IsSQLClean(p_value);
            // Pre-assignment processing hook
            string result = PrevalidationProcess(p_value);
            // Validation hook
            if (validate)
                valid = instance_specific_validator(p_value, inst) && Validate(result);

            if (valid || !validate)
            {
                result = field_specific_action(instance_specific_action(result, inst));
                return PostvalidationProcess(result);
            }
            else
                throw new InvalidFieldDataException(p_value + " is not a valid value for field " + ColumnName);
        }

        public void SetParentContentModel(ContentModel p_parent)
        {
            ParentContentModel = p_parent;
        }

        internal abstract bool _validate(string p_value);

        // All concrete fields must implement a data validation method.
        // Public so that ContentModels can use it.
        internal bool Validate(string p_value)
        {
            return _validate(p_value);
        }

        // Sanitization hook (required for subclasses)
        public static bool IsSQLClean(string value)
        {
            //string pattern = "([0 - 9!@#$%^&*()_+=?/,.;:'`~]+)";      //We only accept upper and lower case letters
            string keywords = "alter|begin|break|checkpoint|commit|" +
                "create|cursor|dbcc|deny|drop|exec|" +
                "execute|insert|go|grant|opendatasource|" +
                "shutdown|sp_|tran|transaction|update|" +
                "while|xp_";    //These keywords are commonly used when trying to perform an SQL code injection

            return !Regex.IsMatch(value.ToLower(), keywords);
        }

        // Pre-validation processing hook (not required for subclasses)
        internal virtual string PrevalidationProcess(string value)
        {
            return value;
        }

        // Post-validation processing hook (not required for subclasses)
        internal virtual string PostvalidationProcess(string value)
        {
            return value;
        }
    }

    public class CharField : ModelField
    {
        protected int max_length;

        public CharField(string p_column_name,
                         int p_max_lenth,
                         bool p_is_primary_key,
                         bool p_is_foreign_key,
                         bool p_is_unique = false,
                         ModelField p_foreign_table_name = null,
                         Func<string, ContentModelInstance, string> p_instance_specific_action = null,
                         Func<string, string> p_field_specific_action = null,
                         Func<string, ContentModelInstance, bool> p_instance_specific_validator = null)
                  : base(p_column_name,
                         "VARCHAR(" + Convert.ToString(p_max_lenth) + ")",
                         p_is_primary_key,
                         p_is_foreign_key,
                         p_is_unique,
                         p_foreign_table_name,
                         p_instance_specific_action,
                         p_field_specific_action,
                         p_instance_specific_validator)
        {
            max_length = p_max_lenth;
        }

        // CharFields require the value to fit inside the defined length
        override internal bool _validate(string value)
        {
            return value.Length <= max_length;
        }
    }

    public class HashField : CharField
    {
        // Set this to the length of the string resulting from the hash implementation we choose.
        private static int _varchar_field_len = 256;
        // TODO: Look into this number. This just came off the top of my head.
        private static int _max_accepted_input_len = 65536;

        public HashField(string p_column_name,
                         bool p_is_primary_key,
                         bool p_is_foreign_key,
                         bool p_is_unique = false,
                         ModelField p_foreign_table_name = null,
                         Func<string, ContentModelInstance, string> p_instance_specific_action = null,
                         Func<string, string> p_field_specific_action = null,
                         Func<string, ContentModelInstance, bool> p_instance_specific_validator = null)
                  : base(p_column_name,
                         _varchar_field_len,
                         p_is_primary_key,
                         p_is_foreign_key,
                         p_is_unique,
                         p_foreign_table_name,
                         p_instance_specific_action,
                         p_field_specific_action,
                         p_instance_specific_validator)
        { }

        override internal bool _validate(string value)
        {
            return value.Length <= _max_accepted_input_len;
        }

        // Hash the value before storing it
        override internal string PostvalidationProcess(string value)
        {
            byte[] bytes = UTF8Encoding.UTF8.GetBytes(value);
            bytes = new SHA256CryptoServiceProvider().ComputeHash(bytes);
            string encrypted_value = Convert.ToBase64String(bytes);
            return encrypted_value;
        }
    }

    public class IntField : ModelField
    {
        public IntField(string p_column_name, 
                        bool p_auto_incrementing,
                        bool p_is_primary_key, 
                        bool p_is_foreign_key,
                        bool p_is_unique = false,
                        ModelField p_foreign_table_name = null,
                        Func<string, ContentModelInstance, string> p_instance_specific_action = null,
                        Func<string, string> p_field_specific_action = null,
                        Func<string, ContentModelInstance, bool> p_instance_specific_validator = null) 
                 : base(p_column_name, 
                        "INTEGER",
                        p_is_primary_key,
                        p_is_foreign_key,
                        p_is_unique,
                        p_foreign_table_name,
                        p_instance_specific_action,
                        p_field_specific_action,
                        p_instance_specific_validator)
        {
            Auto = p_auto_incrementing;
        }

        override internal bool _validate(string p_value)
        {
            try
            {
                int.Parse(p_value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    public class BooleanField : ModelField
    {

        public BooleanField(string p_column_name,
                            bool p_is_primary_key,
                            bool p_is_foreign_key,
                            bool p_is_unique = false,
                            ModelField p_foreign_table_name = null,
                            Func<string, ContentModelInstance, string> p_instance_specific_action = null,
                            Func<string, string> p_field_specific_action = null,
                            Func<string, ContentModelInstance, bool> p_instance_specific_validator = null)
                     : base(p_column_name,
                            "BOOLEAN",
                            p_is_primary_key,
                            p_is_foreign_key,
                            p_is_unique,
                            p_foreign_table_name,
                            p_instance_specific_action,
                            p_field_specific_action,
                            p_instance_specific_validator)
        {

        }

        override internal bool _validate(string p_value)
        {
            try
            {
                bool.Parse(p_value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    public class FloatField : ModelField
    {
        public FloatField(string p_column_name,
                          bool p_is_primary_key,
                          bool p_is_foreign_key,
                          bool p_is_unique = false,
                          ModelField p_foreign_table_name = null,
                          Func<string, ContentModelInstance, string> p_instance_specific_action = null,
                          Func<string, string> p_field_specific_action = null,
                          Func<string, ContentModelInstance, bool> p_instance_specific_validator = null)
                   : base(p_column_name,
                          "REAL",
                          p_is_primary_key,
                          p_is_foreign_key,
                          p_is_unique,
                          p_foreign_table_name,
                          p_instance_specific_action,
                          p_field_specific_action,
                          p_instance_specific_validator)
        {

        }

        override internal bool _validate(string p_value)
        {
            try
            {
                float.Parse(p_value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    public class DateTimeField : ModelField
    {
        public DateTimeField(string p_column_name,
                             bool p_is_primary_key,
                             bool p_is_foreign_key,
                             bool p_is_unique = false,
                             ModelField p_foreign_table_name = null,
                             Func<string, ContentModelInstance, string> p_instance_specific_action = null,
                             Func<string, string> p_field_specific_action = null,
                             Func<string, ContentModelInstance, bool> p_instance_specific_validator = null)
                      : base(p_column_name,
                             "TIMESTAMP",
                             p_is_primary_key,
                             p_is_foreign_key,
                             p_is_unique,
                             p_foreign_table_name,
                             p_instance_specific_action,
                             p_field_specific_action,
                             p_instance_specific_validator)
        { }

        internal override bool _validate(string p_value)
        {
            try
            {
                // Use the Npgsql date time type to determine if the string has a valid format.
                NpgsqlDateTime dt = NpgsqlDateTime.Parse(p_value);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
