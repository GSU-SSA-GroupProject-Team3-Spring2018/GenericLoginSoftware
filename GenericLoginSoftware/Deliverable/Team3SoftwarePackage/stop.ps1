
Write-Output "Stopping Team 3 Login Server"
Write-Output "`n"

Write-Output "Stopping Apache server"
./Apache24/bin/httpd.exe -k stop -n "ApacheServiceTeam3Login"
Write-Output "`n"

Write-Output "Stopping Postgres server"
Write-Output $data_dir
C:\PostgreSQL\pg10\bin\pg_ctl -D"C:\PostgreSQL\data\pg10" stop
Write-Output "`n"