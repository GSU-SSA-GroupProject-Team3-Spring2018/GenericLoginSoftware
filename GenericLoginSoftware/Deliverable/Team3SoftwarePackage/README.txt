NOTE: This software requires a Windows user with admin privileges to install!

PowerShell scripts have been provided for installing, configuring, starting, restarting, stopping, and uninstalling the entire stack used for our login software.

01. cd into this folder
02. Open login.conf and make sure that there is no overlap between used ports on your system and the range of ports configured by [StartingPort, StartingPort + NumberOfWorkers)
03. Set the StaticFilesDirectory setting to point to the ./static/ folder included in this package.
04. Run ./propagate_conf
05. Run ./install
		- This installs Apache as a Windows service. It is uniquely named and will not overwrite an existing Apache installation.
		- This also installs Postgres with an included install wizard. This will overwrite an existing Postgres installation. If you wish to use an existing installation, make sure that
			Postgres is running on port 5432 before running proceeding.
06. Run ./start
07. Hack to your hearts content
08. If you reconfigure anything (Don't unless its to fix the port range, and even then, just change StartingPort. When you are breaking our system you have to use our configuration), run ./restart_apache
09. Use q, quit, or exit to stop the login server
10. When you are done, run ./stop to stop Postgres and Apache and ./uninstall to remove the tech stack from your computer.

Note: If you are already running Apache, this will not overwrite your current installation or configuration files.
