Param(
)

$current_dir=$MyInvocation.MyCommand.Path -replace "\\[^\\]*\.ps1$", "\"
$starting_port
$num_workers

Write-Output "Writing configured worker port numbers to proxy_and_balance.conf"

$i = 0
$split_char = ' '
ForEach($line in Get-Content "./login.conf")
{
	if ($i -eq 0) {
		$split_char = $line.SubString(0, 1)
		$i = 1
	}
	$split_line = $line.Split($split_char, [System.StringSplitOptions]::RemoveEmptyEntries)
	# We only care about two settings when configuring Apache's proxy pass settings
	if ($split_line[0] -eq "StartingPort")
	{
		$starting_port = [int]($split_line[1])
	}
	if ($split_line[0] -eq "NumberOfWorkers")
	{
		$num_workers = [int]($split_line[1])
	}
}

# Beginning of config file contents
$config=@"
<Proxy "balancer://login_backend_worker_cluster">
	ProxySet growth=10
"@

# Write a load balancer entry for each worker setup in login.conf
For ($i=0; $i -lt $num_workers; $i++)
{
	$port = $starting_port + $i
	$config+="`n	BalancerMember `"http://localhost:$($port)/`""
}

# End of config file contents
$config+=@"

</Proxy>

ProxyPass "/" "balancer://login_backend_worker_cluster/"
ProxyPassReverse "/" "balancer://login_backend_worker_cluster/"
"@

# Write the resulting config to proxy_and_balance.conf
($config) | Out-File -FilePath ".\Apache24\conf\auto\proxy_and_balance.conf" -Encoding "UTF8"