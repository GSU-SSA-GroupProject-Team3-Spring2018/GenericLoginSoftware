﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Config
    {
        private static uint _starting_port = 21040;
        public static uint StartingPort { get { return _starting_port; } }

        private static uint _number_of_workers = 5;
        public static uint NumberOfWorkers { get { return _number_of_workers; } }

        private static uint _worker_poll_granularity = 50;
        public static uint WorkerPollGranularity { get { return _worker_poll_granularity; } }

        private static uint _worker_manager_loop_granularity = 50;
        public static uint WorkerManagerLoopGranularity { get { return _worker_manager_loop_granularity; } }

        private static string _four_zero_four = "404.html";
        public static string FourZeroFour { get { return _four_zero_four; } }

        private static string _static_file_directory = "./static/";
        public static string StaticFileDirectory { get { return _static_file_directory; } }

        private static string _database_host_name = "localhost";
        public static string DatabaseHostName { get { return _database_host_name; } }

        private static string _database_username = "admin";
        public static string DatabaseUsername { get { return _database_username; } }

        private static string _database_password = "pass";
        public static string DatabasePassword { get { return _database_password; } }

        private static string _database_name = "dbname";
        public static string DatabaseName { get { return _database_name; } }

        private static string _encryption_passphrase = "pass";
        public static string EncryptionPassphrase { get { return _encryption_passphrase; } }

        private static TimeSpan _session_idle_timeout = new TimeSpan(0, 15, 0);
        public static TimeSpan SessionIdleTimeout { get { return _session_idle_timeout; } }

        private static TimeSpan _session_token_expiration = new TimeSpan(0, 15, 0);
        public static TimeSpan SessionTokenExpiration { get { return _session_token_expiration; } }

        private static int _number_of_fake_cookies = 10;
        public static int NumberOfFakeCookies { get { return _number_of_fake_cookies; } }

        public static string SessionKeyPrefix = "w";

        public static void load_config(string config_file)
        {
            StreamReader fs;
            try
            {
                fs = new StreamReader(config_file);
            }
            catch (Exception) { Console.WriteLine("Invalid file");  return; }

            string line = fs.ReadLine();
            char splitter = line[0];
            while ((line = fs.ReadLine()) != null)
            {
                string[] split = line.Split(new char[] { splitter });
                if (split.Length != 2) throw new InvalidDataException("Invalid config format. Format rule: <key> <value>. <value> must have no spaces.");
                switch (split[0])
                {
                    case "StartingPort":
                        {
                            try
                            {
                                _starting_port = Convert.ToUInt16(split[1]);
                            }
                            catch (FormatException)
                            {
                                throw new InvalidDataException("StartingPort must be a valid positive integer");
                            }
                            catch (OverflowException)
                            {
                                throw new InvalidDataException("StartingPort must be a valid port number (port < 2^16). For proper execution of asynchronous workers, port number should be less than the maximum.");
                            }
                            break;
                        }

                    case "NumberOfWorkers":
                        {
                            try
                            {
                                _number_of_workers = Convert.ToUInt16(split[1]);
                            }
                            catch (FormatException)
                            {
                                throw new InvalidDataException("NumberOfWorkers must be a valid positive integer");
                            }
                            catch (OverflowException)
                            {
                                throw new InvalidDataException("NumberOfWorkers must be < 2^16).");
                            }
                            break;
                        }

                    case "WorkerPollGranularity":
                        {
                            try
                            {
                                _worker_poll_granularity = Convert.ToUInt16(split[1]);
                            }
                            catch (FormatException)
                            {
                                throw new InvalidDataException("WorkerPollGranularity must be a valid positive integer");
                            }
                            catch (OverflowException)
                            {
                                throw new InvalidDataException("WorkerPollGranularity must be a valid UInt16.");
                            }
                            if (_worker_poll_granularity > 500)
                            {
                                Console.Error.WriteLine("Warning: WorkerPollGranularity setting was set to " + Convert.ToString(_worker_poll_granularity) + " ms. Values greater than 500 ms are strongly discouraged. Recommended setting is 50 ms.");
                            }
                            break;
                        }

                    case "WorkerManagerLoopGranularity":
                        {
                            try
                            {
                                _worker_manager_loop_granularity = Convert.ToUInt16(split[1]);
                            }
                            catch (FormatException)
                            {
                                throw new InvalidDataException("WorkerManagerLoopGranularity must be a valid positive integer");
                            }
                            catch (OverflowException)
                            {
                                throw new InvalidDataException("WorkerManagerLoopGranularity must be a valid UInt16.");
                            }
                            if (_worker_manager_loop_granularity > 500)
                            {
                                Console.Error.WriteLine("Warning: WorkerManagerLoopGranularity setting was set to " + Convert.ToString(_worker_manager_loop_granularity) + " ms. Values greater than 500 ms are highly discouraged. Recommended setting is 100 ms.");
                            }
                            break;
                        }

                    case "FourZeroFour":
                        {
                            _four_zero_four = split[1];
                            if (!_four_zero_four.EndsWith(".html"))
                                throw new InvalidDataException("FourZeroFour must specify a .html file.");
                            break;
                        }

                    case "StaticFileDirectory":
                        {
                            _static_file_directory = split[1];
                            break;
                        }

                    case "DatabaseHostName":
                        {
                            _database_host_name = split[1];
                            break;
                        }

                    case "DatabaseUsername":
                        {
                            _database_username = split[1];
                            break;
                        }

                    case "DatabasePassword":
                        {
                            _database_password = split[1];
                            break;
                        }

                    case "DatabaseName":
                        {
                            _database_name = split[1];
                            break;
                        }

                    case "EncryptionPassphrase":
                        {
                            _encryption_passphrase = split[1];
                            break;
                        }

                    case "SessionIdleTimeout":
                        {
                            TimeSpan.TryParse(split[1], out _session_idle_timeout);
                            break;
                        }

                    case "SessionTokenExpiration":
                        {
                            TimeSpan.TryParse(split[1], out _session_idle_timeout);
                            break;
                        }
                }
            }
        }
    }
}
