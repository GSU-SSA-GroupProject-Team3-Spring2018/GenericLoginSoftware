﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Server
{

    class NullCallbackRequestRouteLeafException : Exception
    {
        public NullCallbackRequestRouteLeafException(string message) : base(message) { }
    }

    class NoMatchInRequestRouteException : Exception { }

    class UnacceptableInputException : Exception { }

    class RequestRoute
    {
        protected Regex match_pattern;
        private Func<HttpListenerRequest, ResponseData> callback;
        private List<RequestRoute> sub_routes;

        // A RequestRoute node must have a regex pattern to match against incoming request URLs and can have an action to perform and/or child nodes to propagate the request to
        public RequestRoute(string p_match_pattern, Func<HttpListenerRequest, ResponseData> p_callback = null, RequestRoute[] p_sub_routes = null)
        {
            match_pattern = new Regex(p_match_pattern);

            callback = p_callback;

            if (p_sub_routes == null)
                sub_routes = new List<RequestRoute>();
            else
                sub_routes = p_sub_routes.ToList();

            if (callback == null) callback = (req) => { return new ResponseData(""); };
        }

        // path (string) : The URL path at the current level of routing. Subsequent levels of request routes process out the part of the path that they match.
        // request (HttpListenerRequest) : The request for the URL. Used to access POST information and cookies.
        public ResponseData Route(string path, HttpListenerRequest request)
        {
            // Check that this node matches the path passed
            if (!match_pattern.IsMatch(path))
                throw new NoMatchInRequestRouteException();

            // Remove matching string in path
            path = match_pattern.Replace(path, new MatchEvaluator((match) => { return ""; }));

            // If this route is an internal node, continue routing down its subroutes
            if (sub_routes.Count > 0)
            {
                // Internal nodes can have functionality attached, so we call the callback without returning.
                callback(request);

                // Find a sub_route that matches
                foreach (RequestRoute sub_route in sub_routes)
                {
                    try
                    {
                        // The first matching subroute is passed the request
                        return sub_route.Route(path, request);
                    }
                    catch (NoMatchInRequestRouteException)
                    {
                        // If the subroute did not match, just continue on to the next one
                        continue;
                    }
                }
            }
            // Otherwise, this route is a leaf and we return the result of its callback
            else
            {
                return callback(request);
            }
            
            // No match found, so throw a no match exception
            throw new NoMatchInRequestRouteException();
        }

    }

    // A struct for telling Workers what to return to a client
    struct ResponseData
    {
        public readonly string Content;
        public readonly HttpStatusCode StatusCode;
        public readonly string RedirectURL;
        public Dictionary<string, string> Cookies;

        public ResponseData(string p_content, HttpStatusCode p_status_code = HttpStatusCode.OK, string p_redirect_url = "", Dictionary<string, string> p_cookies = null)
        {
            Content = p_content;
            StatusCode = p_status_code;
            RedirectURL = p_redirect_url;
            Cookies = p_cookies;
        }
    }

    class Router
    {

        private static RequestRoute root;
        private static bool routes_set = false;

        // Cache our 404 content for faster performance
        // TODO: With such a small site, it will probably speed up performance drastically if we cache all content in a Dictionary<string, string> instead of reading from the file system every time.
        private static string four_zero_four_cache;
        static Router()
        {
            four_zero_four_cache = Router.GetStaticFile(Config.FourZeroFour);
        }

        // Gets a file from the configured static files directory
        public static string GetStaticFile(string filename)
        {
            // filename must be inside our static files directory and paths beginning with '/' are absolute
            if (filename.StartsWith("/")) filename = filename.Remove(0, 1);

            return File.ReadAllText(Path.Combine(Config.StaticFileDirectory, filename));
        }

        // Takes a string of the form "key1=val1&key2=val2&key3=val3..." and returns a Dictionary containing the encoded key value pairs
        public static Dictionary<string, string> ParseFormData(string data)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();

            data = Uri.UnescapeDataString(data);
            data = Regex.Unescape(data);

            // Parameters are seperated by '&'
            string[] split = data.Split(new char[] { '&' });

            foreach (string s in split)
            {
                // Values are seperated from keys by '='
                string[] kvpair = s.Split(new char[] { '=' });
                result.Add(kvpair[0], kvpair[1]);
            }

            return result;
        }
        
        public static void SetRoutes(RequestRoute p_root)
        {
            if (!routes_set)
            {
                root = p_root;
                routes_set = true;
            }
        }
        
        // Should use ContentModels when storing or retrieving data
        // Router.route returns the content to be returned for a specific URL
        public static ResponseData Route(HttpListenerRequest request)
        {
            try
            {
                return root.Route(request.Url.AbsolutePath.Remove(0, 1), request);
            }
            catch (NoMatchInRequestRouteException)
            {
                return new ResponseData(
                    four_zero_four_cache,
                    HttpStatusCode.NotFound,
                    ""
                );
            }
            catch (UnacceptableInputException)
            {
                return new ResponseData("", HttpStatusCode.Forbidden, "");
            }

        }

    }

}
