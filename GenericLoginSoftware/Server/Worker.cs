﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    class Worker
    {

        private Thread thread;
        private string[] ips;
        private uint port;

        // Worker._dead is private so that Worker.kill() is enforced as the only way to terminate a Worker
        private bool _dead = false;
        private Mutex _dead_mutex = new Mutex();
        public Mutex DeadMutex { get { return _dead_mutex; } }

        private bool _listener_running = false;
        private int _poll_granularity = (int)Config.WorkerPollGranularity;

        public Worker(string[] p_ips, uint p_port)
        {
            ips = p_ips;
            port = p_port;
            thread = new Thread(Work);
        }

        public override string ToString()
        {
            return "Worker on port " + port.ToString();
        }

        public void Start()
        {
            thread.Start();
        }

        // TODO: Implement urgent.
        public void Kill(bool urgent)
        {
            _dead_mutex.WaitOne();
            _dead = true;
            _dead_mutex.ReleaseMutex();
        }

        // This code executes inside the Worker's thread. It creates an HttpListener listening on the Worker's assigned port
        //  and continually listens on that port, handling 
        private void Work()
        {
            // Create an HttpListener to handle proxied network activity coming in through the localhost on the configured port
            HttpListener http_listener = new HttpListener();
            foreach (string ip in ips)
                http_listener.Prefixes.Add("http://" + ip + ":" + Convert.ToString(port) + "/");
            http_listener.Start();

            _dead_mutex.WaitOne();
            bool local_dead = _dead;
            _dead_mutex.ReleaseMutex();

            while (!local_dead)
            {
                if (!_listener_running)
                {
                    _listener_running = true;
                    // We use asynchronous context checking so that we can poll the thread and shutdown when needed
                    http_listener.GetContextAsync().ContinueWith(async (t) =>
                    {
                        HttpListenerContext context = await t;
                        ProcessRequest(context);
                        // The request has been processed; this worker can now handle a new request.
                        _listener_running = false;
                    });
                }
                else
                {
                    // Poll at interval of _poll_granularity until the listener finishes processing a request
                    Thread.Sleep(_poll_granularity);
                }
                
                // Check to see if the worker should die
                _dead_mutex.WaitOne();
                local_dead = _dead;
                _dead_mutex.ReleaseMutex();
            }

            http_listener.Stop();
        }

        private void ProcessRequest(HttpListenerContext context)
        {
            HttpListenerRequest request = context.Request;
            HttpListenerResponse response = context.Response;

            ResponseData routing_result = Router.Route(request);

            string response_string = routing_result.Content;
            int response_status_code = (int)routing_result.StatusCode;

            if (routing_result.Cookies != null)
            {
                string expire_yesterday = (DateTime.Now - new TimeSpan(1, 0, 0, 0)).ToString("ddd, dd MMM yyyy HH:mm:ss");
                // Remove old cookies
                foreach (Cookie c in context.Request.Cookies)
                {
                    context.Response.Headers.Add("Set-Cookie", c.Name + "=" + c.Value + "; Expires=" + expire_yesterday + "; path=/;");
                }

                // Add new cookies
                foreach (KeyValuePair<string, string> cookie in routing_result.Cookies)
                    context.Response.Headers.Add("Set-Cookie", cookie.Key + "=" + cookie.Value + "; path=/");
            }

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(response_string);
            
            context.Response.StatusCode = response_status_code;
            context.Response.OutputStream.Write(buffer, 0, buffer.Length);
            context.Response.OutputStream.Close();
        }

    }

}
