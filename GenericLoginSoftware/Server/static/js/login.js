﻿
function onReady() {

    function showError(msg) {
        $('#error-msg').text(msg);
        $('#error-wrap').slideDown();
    }

    function success(data) {
        data = JSON.parse(data);

        if ('success' in data) {
            if (data['success']) {
                if ('redirect' in data)
                    location.href = data['redirect'];
            } else {
                if ('message' in data) {
                    let msg = data['message'];
                    if (!msg) msg = 'Invalid username and/or password';
                    showError(msg);
                }
            }
        }
    }

    function onLoginClick() {

        let username = $("#username").val();
        let password = $("#pwd").val();
        let error = $("#error-wrap");

        if (!usernameIsValid(username) || !passwordIsValid(password)) {
            error.text("Invalid username or password");
            error.slideDown();
            return;
        }

        error.slideUp();

        $.ajax({
            method: "POST",
            url: "/api/login",
            data: {
                "username": username,
                "password": password
            },
            error: error,
            success: function (data) { success(data); }
        });

    }

    $("#login-button").click(onLoginClick);

}

$(onReady);
