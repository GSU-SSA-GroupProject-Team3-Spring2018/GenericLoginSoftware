﻿
function emailIsValid(email) {
    let email_regex = /^(([^<>()\[\]\\.,:\s@"]+(\.[^<>()\[\]\\.,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email_regex.test(email);
}

function usernameIsValid(username) {
    // Allow alphanumeric, period, underscore, and hyphen. Must be 4+ characters long
    let username_regex = /[A-z0-9._-]{4,}/;
    return username_regex.test(username);
}

// Validate password formatting on the client-side
function passwordIsValid(password) {
    let allowed_chars_and_length = /^[0-9A-z!\[\]+\\\-:_?]{8,}/;
    let have_lower = /^.*[a-z]/;
    let have_upper = /^.*[A-Z]/;
    let have_number = /^.*[0-9]/;
    return allowed_chars_and_length.test(password) &&
           have_lower.test(password) &&
           have_upper.test(password) &&
           have_number.test(password);
}

function error() {
    alert("An unexpected error occured when trying to access the server...");
}
