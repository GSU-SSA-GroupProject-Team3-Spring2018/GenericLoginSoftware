
$current_dir=$MyInvocation.MyCommand.Path -replace "\\[^\\]*\.ps1$", "\"

Write-Output "`nInstalling Apache 2.4.33 with service name ApacheServiceTeam3Login"
.\Apache24\bin\httpd.exe -k install -n "ApacheServiceTeam3Login"
Write-Output "`n"

Write-Output "Setting Apache ServerRoot to install directory"
$server_root="""$($current_dir)Apache24"""
"Define SRVROOT $($server_root) `nServerRoot ""`${SRVROOT}""" | Out-File -FilePath ".\Apache24\conf\auto\server_root.conf" -Encoding "UTF8"
Write-Output "$($server_root)"
Write-Output "`n"

Write-Output "Installing PostgreSQL (login.conf is used here. If you wish to reconfigure your database setup, you will have to ./uninstall and ./install)"

$hostname
$db_name

$i=0
$split_char=' '
ForEach($line in Get-Content "./login.conf")
{
	if ($i -eq 0)
	{
		$split_char = $line.SubString(0, 1)
		$i=1
	}
	$split_line = $line.Split($split_char, [System.StringSplitOptions]::RemoveEmptyEntries)
	if ($split_line[0] -eq "DatabaseHostname")
	{
		$hostname = $split_line[1]
	}
	if ($split_line[0] -eq "DatabaseName")
	{
		$db_name = $split_line[1]
	}
}

# Start the installer and wait for it to finish before continuing execution
./PostgreSQL-10.3-1-win64-bigsql.exe | Out-Null

Write-Output "Press any key to continue..."

cd "C:\PostgreSQL\pg10\bin"

Write-Output "Creating Team 3 Login Database"
$port = [Environment]::GetEnvironmentVariable("PGPORT", "Machine")
$data_dir = "C:\PostgreSQL\data\pg10"
Write-Output $data_dir
./pg_ctl start -D "C:\PostgreSQL\data\pg10"
# The --no-password option tells Postgres to use %appdata%/postgres/pgpass.conf instead of prompting for a password
./createdb --owner="postgres" --host="$hostname" --port="5432" --username="postgres" --no-password  "$db_name"
$script = $current_dir + "\structure_database.sql"
./psql -h"$hostname" -p"5432" -U"postgres" -f"$script" "$db_name"
./pg_ctl stop -D "C:\PostgreSQL\data\pg10"

# Move back to the original folder
cd $current_dir