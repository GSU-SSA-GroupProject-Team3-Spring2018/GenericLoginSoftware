DROP TABLE IF EXISTS Users,
					 LogInAttempts,
					 Sessions,
					 PreviousPasswords,
					 RecoveryQuestionChoices,
					 RecoveryQuestions CASCADE;

CREATE TABLE Users
(
	id				SERIAL PRIMARY KEY ,
	username		VARCHAR(128) NOT NULL UNIQUE ,
	email			VARCHAR(128) NOT NULL UNIQUE ,
	password_hash   VARCHAR(128) NOT NULL ,
	salt			VARCHAR(64)  NOT NULL
);

CREATE TABLE LogInAttempts
(
	user_id               INTEGER PRIMARY KEY REFERENCES Users (id) ON DELETE CASCADE ,
	ip_address            INET NOT NULL ,
	number_of_attempts    INTEGER ,
	timeout_at            TIMESTAMP
);

CREATE TABLE Sessions
(
	user_id				INTEGER PRIMARY KEY REFERENCES Users (id) ON DELETE CASCADE ,
	key					VARCHAR(16) NOT NULL ,
	token				VARCHAR(32) NOT NULL ,
	refresh_token_at	TIMESTAMP ,
	idle_timeout		TIMESTAMP
);

CREATE TABLE PreviousPasswords
(
	user_id		INTEGER PRIMARY KEY REFERENCES Users (id) ON DELETE CASCADE ,
	password	VARCHAR(128) NOT NULL
);

CREATE TABLE RecoveryQuestionChoices
(
	id			SERIAL PRIMARY KEY ,
	question	VARCHAR(128) NOT NULL
);

CREATE TABLE RecoveryQuestions
(
	user_id						INTEGER NOT NULL REFERENCES Users (id) ON DELETE CASCADE ,
	recovery_question_choice_id	INTEGER NOT NULL REFERENCES RecoveryQuestionChoices (id) ON DELETE SET NULL ,
	answer_hash					CHAR(256) ,

	CONSTRAINT PK_RecoveryQuestions_user_id__recovery_question_choice PRIMARY KEY (user_id, recovery_question_choice_id)
);

INSERT INTO RecoveryQuestionChoices (question) VALUES
	('What is your mother''s maiden name?'),
	('What was the name of your favorite teacher?'),
	('What color was your first car?'),
	('What is your father''s middle name?');
