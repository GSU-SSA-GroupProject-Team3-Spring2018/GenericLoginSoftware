﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ContentModels;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            // TODO: Replace hard-coded config file with command line argument
            Config.load_config("..\\..\\..\\Deliverable\\Team3SoftwarePackage\\login.conf");
            RouteMap.SetRouteMap();
            Server.instance.Configure();
            Server.instance.Run();
        }
    }
}
